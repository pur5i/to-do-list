import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';

import { Observable, of, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { Task } from './interfaces/task';
import { ToDoListService } from './services/to-do-list.service';

enum InputField {
  ITEM = 'item',
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  readonly InputField = InputField;

  @ViewChild('taskForm') taskForm: NgForm;

  reactiveForm: FormGroup;
  tasks$: Observable<Task[]> = of([]);
  includesCompletedTasks: boolean;
  includesIncompleteTasks: boolean;
  ngDestroyed$ = new Subject();

  constructor(private fb: FormBuilder, private toDoService: ToDoListService) {}

  ngOnInit(): void {
    this.createForm();
    this.tasks$ = this.toDoService.getTasks();

    this.tasks$
      .pipe(takeUntil(this.ngDestroyed$))
      .subscribe((tasks: Task[]) => {
        this.includesCompletedTasks = tasks.some((t: Task) => t.complete);
        this.includesIncompleteTasks = tasks.some((t: Task) => !t.complete);
      });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.toDoService.emitStorage();
    });
  }

  ngOnDestroy(): void {
    this.ngDestroyed$.next();
  }

  onSubmit(): void {
    if (this.reactiveForm.valid) {
      this.toDoService.addToDoListItem(
        this.reactiveForm.value[InputField.ITEM]
      );
      this.taskForm.resetForm();
    }
  }

  private createForm(): void {
    this.reactiveForm = this.fb.group({
      [InputField.ITEM]: ['', Validators.required],
    });
  }
}
