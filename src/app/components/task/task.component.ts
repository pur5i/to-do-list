import { Component, OnInit, Input } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';

import { Task } from 'src/app/interfaces/task';
import { ToDoListService } from 'src/app/services/to-do-list.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
})
export class TaskComponent implements OnInit {
  @Input() task: Task;
  constructor(private toDoService: ToDoListService) {}

  ngOnInit(): void {}

  onTaskChangeHandler(id: number, event: MatCheckboxChange): void {
    this.toDoService.updateTaskState(event.checked, id);
  }

  onRemoveTask(id: number) {
    this.toDoService.removeTaskFromList(id);
  }
}
