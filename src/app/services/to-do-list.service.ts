import { Injectable } from '@angular/core';

import { Subject, Observable } from 'rxjs';

import { Task } from '../interfaces/task';

enum LocalStorageKey {
  TO_DO_LIST = 'toDoList',
}

@Injectable({
  providedIn: 'root',
})
export class ToDoListService {
  private subject = new Subject<Task[]>();
  tasks: Task[] = [];

  constructor() {}

  addToDoListItem(task: string) {
    const tasks = this.getTasksFromLocalStorage();
    tasks.unshift({
      id: Date.now(),
      name: task,
      complete: false,
    });
    localStorage.setItem(LocalStorageKey.TO_DO_LIST, JSON.stringify(tasks));
    this.emitStorage();
  }

  getTasksFromLocalStorage(): Task[] {
    return JSON.parse(localStorage.getItem(LocalStorageKey.TO_DO_LIST)) || [];
  }

  updateTaskState(state: boolean, id: number) {
    const tasks = this.getTasksFromLocalStorage().map((task) => {
      if (task.id === id) {
        task.complete = state;
      }
      return task;
    });
    this.updateToDoList(tasks);
  }

  removeTaskFromList(id: number) {
    const tasks = this.getTasksFromLocalStorage().filter(
      (task) => task.id !== id
    );
    this.updateToDoList(tasks);
  }

  emitStorage() {
    this.subject.next(this.getTasksFromLocalStorage());
  }

  getTasks(): Observable<Task[]> {
    return this.subject.asObservable();
  }

  private updateToDoList(tasks: Task[]) {
    localStorage.setItem(LocalStorageKey.TO_DO_LIST, JSON.stringify(tasks));
    this.emitStorage();
  }
}
